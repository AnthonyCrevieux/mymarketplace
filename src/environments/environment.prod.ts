export const environment = {
  production: true,
  backendSpringUsers: {
    baseURL:"https://spring-user-prod.herokuapp.com/user"
  },
  backendSpringPortfolio: {
    baseURL:"https://spring-portfolio-prod.herokuapp.com/portfolio"
  },
  backendSpringDashboard: {
    baseURL:"https://spring-dashboard-prod.herokuapp.com/dashboard"
  },
  backendNodeJS: {
    baseURL:"https://node-pipeline-prod.herokuapp.com/mybourse-api"
  }
};

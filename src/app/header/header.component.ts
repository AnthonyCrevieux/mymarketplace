import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../service/authorization.service';
import { UnsubscribeComponent } from '../unsubscribe/unsubscribe.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public loginService: AuthorizationService) { }

  currentUser = sessionStorage.getItem('username');

  ngOnInit(): void {
  }

}

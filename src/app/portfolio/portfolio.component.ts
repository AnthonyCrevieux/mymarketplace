import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Portfolio } from '../model/portfolio';
import { Stock } from '../model/stock';
import { PortfolioService } from '../service/portfolio.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor(private _portfolio: PortfolioService,
              private router: Router) { }

  portfolios : Portfolio[] = [];
  stock: Stock = new Stock();
  todayStock: Stock = new Stock();
  delta=0;
  calculDeltaRounded="";
  performance=0;
  performancePercentage="";
  noPortfolio = false;
  
  ngOnInit(): void {
    this.getUserPortfolio();
  }

  getUserPortfolio(){
    this._portfolio.getUserPortfolio(sessionStorage.getItem('username')).subscribe((data) => {
      this.portfolios = data;
      if(this.portfolios.length === 0)
        this.noPortfolio = true;
      for(let portfolio of this.portfolios){
        this._portfolio.getTodaySpecificStock(portfolio.stockName).subscribe((todayData) => {
          this.todayStock = todayData;
          portfolio.price = this.todayStock.close;
          if(portfolio.purchaseDate === this.getTodayDate())
            portfolio.purchasePrice = portfolio.price;
          this._portfolio.getSpecificStock(portfolio.stockName, portfolio.purchaseDate).subscribe((data) => {
            this.stock = data;
            portfolio.purchasePrice = this.stock.close;
            this.delta = (Number(portfolio.price) - Number(portfolio.purchasePrice))/Number(portfolio.price)*100;
            this.calculDeltaRounded = this.roundTo(this.delta,2) + "%";
            portfolio.delta = this.calculDeltaRounded;
            portfolio.performance = Number(portfolio.price) - Number(portfolio.purchasePrice);
            this.performance = this.performance + portfolio.performance;
            if(this.performance>=0)
              this.performancePercentage = "+" + this.roundTo(this.performance,2) + "$";
            else
              this.performancePercentage = this.roundTo(this.performance,2) + "$";
          });
        });
      }
    });
  }

  onTrash(id:Number) {
    this._portfolio.deletePortfolio(id).subscribe(
      (res) => {
        console.log(res);
        window.location.reload();
      });
  }

  roundTo(n: any, digits: number) {
    var negative = false;
    if (digits === undefined) {
        digits = 0;
    }
    if (n < 0) {
        negative = true;
        n = n * -1;
    }
    var multiplicator = Math.pow(10, digits);
    n = parseFloat((n * multiplicator).toFixed(11));
    n = (Math.round(n) / multiplicator).toFixed(digits);
    if (negative) {
        n = (n * -1).toFixed(digits);
    }
    return n;
}

getYesterdayDate(){
    var today = new Date();
    var dd = String(today.getDate()-1).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    var yesterday = today.toDateString();
    yesterday = yyyy + '-' + mm + '-' + dd;
    return yesterday;
}

getTodayDate(){
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();
  var todayDate = today.toDateString();
  todayDate = yyyy + '-' + mm + '-' + dd;
  return todayDate;
}

}

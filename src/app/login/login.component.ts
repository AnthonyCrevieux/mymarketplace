import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { AuthorizationService } from '../service/authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  username = '';
  password = '';
  message = "";

  constructor(private router: Router,
              private _auth: AuthorizationService) { }

  ngOnInit(): void {
  }

  checkLogin() {
    this._auth.signIn(this.username, this.password).subscribe(
      res => {
        sessionStorage.setItem('username', this.username);
        this.message = "You have successfully logged in";
        this.router.navigate([''])
      },
      error => {
        this.message = "Your credentials are incorrect";
      }
    );
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Stock } from '../model/stock';
import { HomeService } from '../service/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _home: HomeService) { }

  ngOnInit(): void {
    this.getBestPerfs();
  }

  stockArray: Stock [] = [];
  stockArrayFiltered: Stock [] = [];
  stockArrayFirst: Stock [] = [];
  stockArraySecond: Stock [] = [];
  stockArrayThird: Stock [] = [];

  getBestPerfs() {
    this._home.getLastBestPerformances().subscribe((stockList) => {
      this.stockArray = stockList;
      for(let i=3; i<= 9; i++){
        this.stockArrayFiltered.push(this.stockArray[i]);
      }
      for(let j=0; j<= 2; j++){
        if(j === 0)
          this.stockArrayFirst.push(this.stockArray[j]);
        else if(j === 1)
          this.stockArraySecond.push(this.stockArray[j]);
        else
          this.stockArrayThird.push(this.stockArray[j]);
      }
    });
  }

}

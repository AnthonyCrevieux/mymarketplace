import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../service/authorization.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private _auth: AuthorizationService,
              private router: Router) { }

  ngOnInit(): void {
    this._auth.logOut();
    this.router.navigate(['']);
  }

}

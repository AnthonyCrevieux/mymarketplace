import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserListComponent } from './user-list/user-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from '@angular/forms';
import { UserlistService } from './service/userlist.service';
import { AuthorizationService } from './service/authorization.service';
import { LogoutComponent } from './logout/logout.component';
import { PortfolioService } from './service/portfolio.service';
import { DashboardService } from './service/dashboard.service';
import { ChartsModule } from 'ng2-charts';
import { HomeService } from './service/home.service';
import { UnsubscribeComponent } from './unsubscribe/unsubscribe.component';
import { ProfilComponent } from './profil/profil.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    UserListComponent,
    DashboardComponent,
    PortfolioComponent,
    FooterComponent,
    LogoutComponent,
    UnsubscribeComponent,
    ProfilComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ChartsModule,
    BrowserAnimationsModule,
    NzMessageModule
  ],
  providers: [UserlistService,AuthorizationService,PortfolioService,DashboardService,HomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }

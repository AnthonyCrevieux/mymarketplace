import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserlistService {

  _apiBaseUrl = environment.backendSpringUsers.baseURL;

  constructor(private http: HttpClient) {
  }
  
  public findAllUsers(): Observable<User[]> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    return this.http.get<User[]>(this._apiBaseUrl + '/listerLesUsers',{headers});
  }

  public deleteUser(id: Number) : Observable<User> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    return this.http.delete<any>(this._apiBaseUrl + "/deleteUser/" + id);
  }
}

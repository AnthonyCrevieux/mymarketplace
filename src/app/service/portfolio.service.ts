import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Portfolio } from '../model/portfolio';
import { Stock } from '../model/stock';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {
  _apiNodeUrl = environment.backendNodeJS.baseURL;
  _apiSpringUrl = environment.backendSpringPortfolio.baseURL;

  constructor(private http: HttpClient) {
   }

  public getUserPortfolio (username: string | null): Observable<any>{
    return this.http.get<any>(this._apiSpringUrl + '/listePreferences/' + username);
  }

  public getTodaySpecificStock (stockName: string): Observable<Stock> {
    return this.http.get<Stock>(this._apiNodeUrl + '/portfolio/' + stockName);
  }

  public getSpecificStock (stockName: string, datetime: string): Observable<Stock> {
    return this.http.get<Stock>(this._apiNodeUrl + '/portfolio/' + stockName + '/' + datetime);
  }

  public addToPortfolio(username:string, stockName:string):Observable<Portfolio> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    return this.http.post<Portfolio>(this._apiSpringUrl + '/ajouterStock/' + stockName + '/' + username, {headers});
  }

  public deletePortfolio(id: Number) : Observable<Portfolio> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    return this.http.delete<Portfolio>(this._apiSpringUrl + "/retirerStock/" + id);
  }

  public getUserPortfolioSpecificStock(username: string, stockName: string): Observable<any> {
    return this.http.get<any>(this._apiSpringUrl + "/findStock/" + username + '/' + stockName)
  }

  public deleteEntirePortfolio(username: string): Observable<Portfolio[]> {
    return this.http.delete<Portfolio[]>(this._apiSpringUrl + '/supprimerPortfolio/' + username);
  }

}

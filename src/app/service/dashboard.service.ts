import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Stock } from '../model/stock';
import { Macd } from '../model/macd';
import { Dashboard } from '../model/dashboard';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  _apiNodeUrl = environment.backendNodeJS.baseURL;
  _apiSpringUrl = environment.backendSpringDashboard.baseURL;

  constructor(private http: HttpClient) { }

  public getUserDashboard (username: string | null): Observable<Dashboard[]>{
    return this.http.get<Dashboard[]>(this._apiSpringUrl + '/listePreferences/' + username);
  }

  public deleteDashboard(stockName: string, userName: string) : Observable<Dashboard> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    return this.http.delete<Dashboard>(this._apiSpringUrl + '/retirerStock/' + stockName + '/' + userName);
  }

  public getSpecificStock (stockName: string): Observable<Stock> {
    return this.http.get<Stock>(this._apiNodeUrl + '/dashboard/' + stockName);
  }

  public getSpecificStockList (stockName: string): Observable<Stock[]> {
    return this.http.get<Stock[]>(this._apiNodeUrl + '/dashboard/chart/' + stockName);
  }

  public getSpecificStockMacd (stockName: string): Observable<Macd> {
    return this.http.get<Macd>(this._apiNodeUrl + '/macd/' + stockName);
  }

  public addToDashboard(username:string, stockName:string):Observable<Dashboard> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    return this.http.post<Dashboard>(this._apiSpringUrl + '/ajouterStock/' + stockName + '/' + username, {headers});
  }

  public deleteEntireDashboard(username: string): Observable<Dashboard[]> {
    return this.http.delete<Dashboard[]>(this._apiSpringUrl + '/supprimerDashboard/' + username);
  }

}

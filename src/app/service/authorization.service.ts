import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  _apiBaseUrl = environment.backendSpringUsers.baseURL;

  registerUser : any;
  users : User[] = [];
  public user: User = new User();
  isUserWellRegistered = "Yes";

  constructor(private http: HttpClient,
              private router: Router) {
  }

  public register(username:string, email:string, password:string) {
    return this.createUser(username, email, password);
  }

  public signIn(username: string, password: string) {
    return this.getUserAuthenticated(username, password);
  }

  public isLoggedIn() {
    let username = sessionStorage.getItem('username')
    if(username != "Anthony")
      return !(username === null)
    else
      return this.isAdminLoggedIn();
  }

  public isAdminLoggedIn() {
    let userAdmin = sessionStorage.getItem('username')
    if(userAdmin === "Anthony")
      return !(userAdmin === null)
    else
      return null;
  }

  public logOut() {
    sessionStorage.removeItem('username')
    this.router.navigate(['']);
  }

  public createUser(username:string, email:string, password:string):Observable<User> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    let creatingUser: User = new User();
    creatingUser.identifiant = username;
    creatingUser.email = email;
    creatingUser.password = password;
    let body = JSON.stringify(creatingUser);
    return this.http.post<User>(this._apiBaseUrl + '/register', body, {headers});
  }

  public getUserAuthenticated(username: string, password: string): Observable<User> {
    const headers = new HttpHeaders().set("Content-type", "application/json");
    let user: User = new User();
    user.identifiant = username;
    user.password = password;
    let body = JSON.stringify(user);
    return this.http.post<User>(this._apiBaseUrl + '/signin', body, {headers});
  }

  public unsubscribe(username: string) {

    return this.http.delete(this._apiBaseUrl + '/unsubscribe/' + username);
  }

  public updateProfil(id:Number, username:string, email:string, password:string): Observable<User>{
    const headers = new HttpHeaders().set("Content-type", "application/json");
    let updatingUser: User = new User();
    updatingUser.id = id;
    updatingUser.identifiant = username;
    updatingUser.email = email;
    updatingUser.password = password;
    let body = JSON.stringify(updatingUser);
    return this.http.put<User>(this._apiBaseUrl + '/modifierUser', body, {headers});
  }

  public findUser(username: string): Observable<User> {
    return this.http.get<User>(this._apiBaseUrl + '/findUserByIdentifiant/' + username);
  }

}

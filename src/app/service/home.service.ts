import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Stock } from '../model/stock';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  _apiNodeUrl = environment.backendNodeJS.baseURL;

  constructor(private http: HttpClient) { }

  public getLastBestPerformances(): Observable<Stock[]> {
    return this.http.get<Stock[]>(this._apiNodeUrl + '/home');
  }

}

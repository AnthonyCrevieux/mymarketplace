import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { UserlistService } from '../service/userlist.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private _userList:UserlistService,
              private router: Router) { }

  ngOnInit(): void {
    this.getUsers();
  }

  users : User[] = [];

  //Get all users
  getUsers(){
    this._userList.findAllUsers().subscribe((data) => {this.users = data});
  }

  //Delete user
  onTrash(id:Number) {
    this._userList.deleteUser(id).subscribe(
      res => {
        console.log(res);
        this.getUsers();
      });
  }

}

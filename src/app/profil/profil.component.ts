import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../service/authorization.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  constructor(public auth: AuthorizationService,
              private router: Router) { }

  username = '';
  email = '';
  password = '';
  id:Number = 0;
  confirmPassword = '';
  message: string = "";
  currentUser = sessionStorage.getItem('username');
  
  ngOnInit(): void {
    this.getUserProfil(this.currentUser);
  }

  getUserProfil(username: string) {
    this.auth.findUser(username).subscribe((user) => {
      this.id = user.id;
      this.username = user.identifiant;
      this.email = user.email;
      this.password = user.password;
    })
  }

  update() {
    if(this.checkingPassword(this.password, this.confirmPassword)){
      this.auth.updateProfil(this.id, this.username, this.email, this.password).subscribe(
        user => {
          sessionStorage.setItem('username', this.username);
          this.message = "Your profil has been well updated";
        setTimeout(() => 
        {
          this.getUserProfil(this.username);
        },
        600);
      });
    }
    else
      this.message = "Password and confirm password are different";
  }

  cancel() {
    this.router.navigate(['']);
  }

  checkingPassword(password: string, confirmPassword: string){
    if(password != confirmPassword){
      this.message = "Password and confirm password are different";
      return false;
    }
    else
      return true;
  }
}

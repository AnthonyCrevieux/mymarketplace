import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { RegisterComponent } from './register/register.component';
import { UserListComponent } from './user-list/user-list.component';
import { AuthGuardService } from './service/auth-guard.service';
import { UnsubscribeComponent } from './unsubscribe/unsubscribe.component';
import { ProfilComponent } from './profil/profil.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent,canActivate:[AuthGuardService]},
  {path: 'register', component: RegisterComponent},
  {path: 'unsubscribe', component: UnsubscribeComponent,canActivate:[AuthGuardService]},
  {path: 'profil', component: ProfilComponent,canActivate:[AuthGuardService]},
  {path: 'dashboard', component: DashboardComponent,canActivate:[AuthGuardService]},
  {path: 'portfolio', component: PortfolioComponent,canActivate:[AuthGuardService]},
  {path: 'userlist', component: UserListComponent,canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

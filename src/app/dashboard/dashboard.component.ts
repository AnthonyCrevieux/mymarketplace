import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Chart from 'chart.js';
import { ChartDataSets } from 'chart.js';
import { Dashboard } from '../model/dashboard';
import { Linechart } from '../model/linechart';
import { Macd } from '../model/macd';
import { Stock } from '../model/stock';
import { DashboardService } from '../service/dashboard.service';
import { PortfolioService } from '../service/portfolio.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Label } from 'ng2-charts';
import { Portfolio } from '../model/portfolio';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _dashboard: DashboardService,
              private _portfolio: PortfolioService,
              private nzMessage: NzMessageService,
              private router: Router) { }
  //Dashboard and MACD
  dashboards: Dashboard [] = [];
  portfolios: Portfolio [] = [];
  dashboard: Stock = new Stock();
  macd: Macd = new Macd();
  dashboardsChart: Stock [] = [];
  noDashboard = false;
  addedToDashboard = false;
  alreadyInDashboard = false;
  addedToPortfolio = false;
  alreadyInPortfolio = false;

  //Chart
  lineChartData: ChartDataSets[] = [];
  priceArray = [];
  lineChartLabels: Label[] = [];
  dateArray = [];
  lineChart: Linechart = new Linechart();

  //List of stocks in DB
  stocks = [
    {},
    {"symbol":"ATVI","name":"Activision Blizzard Inc"},
    {"symbol":"ADBE","name":"Adobe Inc"},
    {"symbol":"GOOG","name":"Alphabet Inc"},
    {"symbol":"AMZN","name":"Amazon.com Inc"},
    {"symbol":"AAL","name":"American Airlines Group Inc"},
    {"symbol":"AXP","name":"American Express Co"},
    {"symbol":"AAPL","name":"Apple Inc"},
    {"symbol":"BIIB","name":"Biogen Inc"},
    {"symbol":"BA","name":"Boeing Co"},
    {"symbol":"BKNG","name":"Booking Holdings Inc"},
    {"symbol":"CAT","name":"Caterpillar Inc"},
    {"symbol":"KO","name":"Coca-Cola Co"},
    {"symbol":"EBAY","name":"eBay Inc"},
    {"symbol":"EA","name":"Electronic Arts Inc"},
    {"symbol":"FB","name":"Facebook Inc"},
    {"symbol":"GS","name":"Goldman Sachs Group Inc"},
    {"symbol":"HAS","name":"Hasbro Inc"},
    {"symbol":"HON","name":"Honeywell International Inc"},
    {"symbol":"INTC","name":"Intel Corp"},
    {"symbol":"IBM","name":"International Business Machines Corp"},
    {"symbol":"JPM","name":"JPMorgan Chase & Co"},
    {"symbol":"MCD","name":"McDonald's Corp"},
    {"symbol":"MRK","name":"Merck & Co Inc"},
    {"symbol":"MSFT","name":"Microsoft Corp"},
    {"symbol":"MDLZ","name":"Mondelez International Inc"},
    {"symbol":"NFLX","name":"Netflix Inc"},
    {"symbol":"PYPL","name":"PayPal Holdings Inc"},
    {"symbol":"PEP","name":"PepsiCo Inc"},
    {"symbol":"SBUX","name":"Starbucks Corp"},
    {"symbol":"TSLA","name":"Tesla Inc"},
    {"symbol":"TRV","name":"Travelers Cos Inc"},
    {"symbol":"UNH","name":"UnitedHealth Group Inc"},
    {"symbol":"WMT","name":"Walmart Inc"},
    {"symbol":"DIS","name":"Walt Disney Co"}
  ];
  //Add to dashboard
  selectedStock: string = "";
  message: string = "";

  //Logged in user
  currentUser = sessionStorage.getItem('username');

  ngOnInit(): void {
    this.getUserDashboard();
  }

  getUserDashboard(){
    this._dashboard.getUserDashboard(this.currentUser).subscribe((data) => {
      this.dashboards = data;
      if(this.dashboards.length === 0)
        this.noDashboard = true;
      for(let dashboard of this.dashboards){
        this._dashboard.getSpecificStock(dashboard.stockName).subscribe((stockData) => {
          this.dashboard = stockData;
          dashboard.symbol = stockData.symbol;
          dashboard.stockName = stockData.name;
          dashboard.price = stockData.close;
          dashboard.open = stockData.open;
          dashboard.low = stockData.low;
          dashboard.high = stockData.high;
          dashboard.volume = stockData.volume.toLocaleString();
          dashboard.delta = stockData.delta;
          this._dashboard.getSpecificStockMacd(dashboard.symbol).subscribe((macdData) => {
            this.macd = macdData;
            dashboard.advise = macdData.advise;
          });
          this._dashboard.getSpecificStockList(dashboard.symbol).subscribe((stockData) => {
            this.dashboardsChart = stockData;
            this.lineChart = new Linechart();
            this.lineChartLabels = [];
            this.lineChartData = [];
            this.lineChart.priceArray = [];
            this.lineChart.dateArray = [];
            for(let i=this.dashboardsChart.length-15; i<=this.dashboardsChart.length; i++){
              this.dashboard = new Stock();
              this.dashboard.close = this.dashboardsChart[i].close;
              this.dashboard.datetime = this.dashboardsChart[i].datetime;
              this.lineChart.priceArray.push(this.dashboard.close);
              this.lineChart.dateArray.push(this.dashboard.datetime);
              this.lineChartData = [{data: this.lineChart.priceArray, label: dashboard.stockName}];
              this.lineChartLabels = this.lineChart.dateArray;

              let canvas: any = document.getElementById(dashboard.stockName);
              let chart = new Chart(canvas, {
                 type: 'line',
                 data: {
                   labels: this.lineChartLabels,
                   datasets: [{
                     data: this.lineChart.priceArray,
                     label: dashboard.symbol,
                     borderColor: "black",
                     backgroundColor: "rgba(207,120,21,0.28)",
                     fill: true
                   }]
                 }
               });
            }
          });
        });
      }
    });
  }

  buy(stockName:string) {
    this._portfolio.getUserPortfolioSpecificStock(this.currentUser, stockName).subscribe((data) => {
          this._portfolio.addToPortfolio(this.currentUser, stockName).subscribe((data) => {
            this.getUserDashboard();
            this.nzMessage.success('Successfully added to portfolio');
          });
        }, (error) => {
          this.getUserDashboard();
          this.nzMessage.error('Already added to portfolio');
        });
  }


  removeDashboard(stockName:string){
    this._dashboard.deleteDashboard(stockName, this.currentUser).subscribe((data) => {
      this.getUserDashboard();
    });
  }

  addToDashboard(selectedStock:string) {
    this._dashboard.getUserDashboard(this.currentUser).subscribe((data) => {
      this.dashboards = data;
        for(let dashboard of this.dashboards){
          if(selectedStock === dashboard.stockName){
            this.getUserDashboard();
            this.nzMessage.error('Already added to dashboard');
          }
          else if(selectedStock === "")
          this.getUserDashboard();
        }
          this._dashboard.addToDashboard(this.currentUser, selectedStock).subscribe((data) => {
            this.noDashboard = false;
            this.getUserDashboard();
          });
    });
  }

  onSelect(event: any) {
      this.selectedStock = String(event.target.value).slice(0,4).trim();
    }

}

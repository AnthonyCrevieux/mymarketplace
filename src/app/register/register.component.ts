import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../service/authorization.service';
import { UserlistService } from '../service/userlist.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  confirmRegistration = false;
  isWellRegistered = false;
  username = '';
  email = '';
  password = '';
  confirmPassword = '';
  message: string = "";

  constructor(public auth: AuthorizationService,
              public _userList: UserlistService) { }

  register() {
    if(this.checkingPassword(this.password, this.confirmPassword)){
      this.auth.register(this.username, this.email, this.password).subscribe(
        res => {
          this.message="";
          this.isWellRegistered = true;
        },
        error => {
          this.message = "This user already exists";
        });
    }
    else 
      this.message = "Password and confirm password are different";
  }

  checkingPassword(password: string, confirmPassword: string){
    if(password != confirmPassword){
      this.message = "Password and confirm password are different";
      return false;
    }
    else
      return true;
  }

  ngOnInit(): void {
  }

}

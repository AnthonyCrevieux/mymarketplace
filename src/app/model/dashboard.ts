export class Dashboard {
    public id: Number =0;
    public symbol : string ="";
    public stockName : string ="";
    public datetime : string ="";
    public price : number =0;
    public open : number =0;
    public low : number =0;
    public high : number =0;
    public volume : string ="";
    public advise : string ="";
    public delta : string ="";
}
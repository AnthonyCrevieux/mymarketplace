export class Macd {
    public id: Number =0;
    public symbol : string ="";
    public datetime : string ="";
    public macd : number =0;
    public macdSignal : number =0;
    public macdHist : number =0;
    public advise : string ="";
}
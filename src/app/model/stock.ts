export class Stock {
    public id: Number =0;
    public symbol : string ="";
    public name : string ="";
    public datetime : string ="";
    public open : number =0;
    public high : number =0;
    public low : number =0;
    public close : number =0;
    public volume : number =0;
    public delta : string ="";
    public percent_change: string="";
}
export class Portfolio {
    public id: Number =0;
    public purchaseDate : string ="";
    public purchasePrice : number =0;
    public stockName : string ="";
    public price : number =0;
    public performance: number =0;
    public delta : string ="";
    public userName : string ="";
}
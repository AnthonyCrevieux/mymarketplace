import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../service/authorization.service';
import { DashboardService } from '../service/dashboard.service';
import { PortfolioService } from '../service/portfolio.service';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.css']
})
export class UnsubscribeComponent implements OnInit {

  constructor(public auth: AuthorizationService,
              private dashboard: DashboardService,
              private portfolio: PortfolioService,
              private router: Router) { }

  @ViewChild('openModal') private openModal;
  
  currentUser = sessionStorage.getItem('username');

  ngOnInit(): void {
    this.openModalOnInit();
  }

  unsubscribeOk () {
      this.dashboard.deleteEntireDashboard(this.currentUser).subscribe(
        req => {
        console.log(req);
      });
      this.portfolio.deleteEntirePortfolio(this.currentUser).subscribe(
        req => {
        console.log(req);
      });
      this.auth.unsubscribe(this.currentUser).subscribe(
        res => {
        console.log(res);
      });
      sessionStorage.removeItem('username');
      this.router.navigate(['']);
      window.location.reload();
  }

  unsubscribeCancel() {
    this.router.navigate(['']);
  }

  openModalOnInit() {
    this.openModal.open(this.openModal);
  }

}
